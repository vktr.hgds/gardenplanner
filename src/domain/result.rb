class Result
  attr_accessor :area, :water_amount, :area_ok, :water_ok

  def initialize(area, water_amount, area_ok, water_ok)
    @area = area
    @water_amount = water_amount
    @area_ok = area_ok
    @water_ok = water_ok
  end

  def show
    puts '*** Result ***'
    puts "Required area: #{area} m2"
    puts "Water needed: #{water_amount} l"
    return puts 'Plant is feasible in your garden! :)' if area_ok && water_ok

    puts 'Plant is NOT feasible in your garden! :('
    puts '- Not enough area.' unless area_ok
    puts '- Not enough water.' unless water_ok
  end
end