class PlantType
  attr_accessor :name, :area, :water_amount

  def initialize(name, area, water_amount)
    @name = name
    @area = area
    @water_amount = water_amount
  end
end