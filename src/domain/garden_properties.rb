class GardenProperties
  attr_accessor :area, :water_supply

  def initialize(area, water_supply)
    @area = area
    @water_supply = water_supply
  end
end