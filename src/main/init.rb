require_relative '../service/garden_service'
require_relative '../domain/garden_properties'
require_relative '../domain/result'
require_relative '../utils/validator'

class MainClass
  def self.items
    puts "\nPlease enter the plant you would like to put in your garden. Please press q when you are done."
    input_items = {}

    loop do
      print 'Enter plant (format: name, amount): '
      value_string = gets.chomp.delete(' ')
      break if value_string.eql? 'q'

      values = value_string.split(',')
      next unless Utils::Validator.float?(values.last)

      input_items[values.first] = values.last.to_f
    end

    puts
    input_items
  end

  def self.garden_properties
    puts 'Please enter your garden properties.'
    print 'Size (square meter): '
    size = gets.chomp
    raise TypeError.new("\nIllegal input format") unless Utils::Validator.float?(size)

    print "Water supply (in liter): "
    water_supply = gets.chomp
    raise TypeError.new("\nIllegal input format") unless Utils::Validator.float?(water_supply)

    GardenProperties.new(size.to_f, water_supply.to_f)
  end

  def self.show(plant_types)
    plant_types.each { |line| puts "- #{line&.split(' ')&.first}" }
  end
end

# ---------------------------------------------------------------------------------------

puts "*** Welcome to Garden Planner ***\n\n"

garden_properties = MainClass.garden_properties
garden_service = GardenService.new

begin
  garden_service.set_garden_properties(garden_properties)
  MainClass.show(garden_service.plant_types)
  result = garden_service.evaluate(MainClass.items)
  result.show
rescue TypeError => e
  puts e.message
end




