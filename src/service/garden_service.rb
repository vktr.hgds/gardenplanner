require_relative '../domain/garden_properties'
require_relative '../domain/plant_type'
require_relative '../domain/result'
require_relative '../utils/validator'
require_relative '../repository/plant_repository'

class GardenService

  attr_accessor :garden_properties

  def plant_types
    PlantRepository.find_all
  end

  def set_garden_properties(garden_prop)
    @garden_properties = garden_prop
  end

  def evaluate(items)
    plants = calculate_and_get_plan(items)
    all_area = plants.reduce(0) { |sum, plant| sum + plant.area }
    all_water_amount = plants.reduce(0) { |sum, plant| sum + plant.water_amount }

    Result.new(
      all_area,
      all_water_amount,
      all_area <= @garden_properties.area,
      all_water_amount <= @garden_properties.water_supply
      )
  end

  private

  def calculate_and_get_plan(items)
    plants = []
    db_items = plant_types

    items.each do |key, value|
      plant = db_items
                &.detect { |db_item| db_item.split(' ').first.eql? key.to_s }
                &.split(' ')

      raise TypeError.new("Unknown plant: #{key}") if plant.nil?

      name = plant.first
      area = plant[1].to_f * value.to_f
      water_amount = area * plant.last.to_f
      plants << PlantType.new(name, area, water_amount)
    end

    plants
  end
end