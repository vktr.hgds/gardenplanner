class PlantRepository
  def self.find_all
    [
      'Corn 0.4 10',
      'Pumpkin 2 5',
      'Grape 3 5',
      'Tomato 0.3 10',
      'Cucumber 0.4 10'
    ]
  end
end
